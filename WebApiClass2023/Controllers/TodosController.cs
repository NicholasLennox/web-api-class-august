﻿using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApiClass2023.Controllers
{
    [Route("api/v1/todos")]
    [ApiController]
    public class TodosController : ControllerBase
    {
        // Static is so that when we create a new instance of the controller its not reset.
        // Controllers have instances created for each request.
        private static List<Todo> _todos = new List<Todo>
        {
            new Todo { Id = 1, Title = "Buy groceries", Description = "Milk, eggs, bread" },
            new Todo { Id = 2, Title = "Finish homework", Description = "Math and science assignments" },
            new Todo { Id = 3, Title = "Walk the dog", Description = "Take Fido for a walk in the park" },
            new Todo { Id = 4, Title = "Plan vacation", Description = "Research destinations and book flights" },
            new Todo { Id = 5, Title = "Read a book", Description = "Start reading 'The Catcher in the Rye'" },
            new Todo { Id = 6, Title = "Write blog post", Description = "Topic: Minimal APIs in .NET 6" },
            new Todo { Id = 7, Title = "Exercise", Description = "Go for a 30-minute jog" },
            new Todo { Id = 8, Title = "Call mom", Description = "Check in and catch up" },
            new Todo { Id = 9, Title = "Clean the garage", Description = "Organize tools and equipment" },
            new Todo { Id = 10, Title = "Prepare for meeting", Description = "Review presentation slides" },
            new Todo { Id = 11, Title = "Fix leaking faucet", Description = "Get a plumbing wrench" },
            new Todo { Id = 12, Title = "Update resume", Description = "Add recent work experience" },
            new Todo { Id = 13, Title = "Watch a movie", Description = "Genre: Action" },
            new Todo { Id = 14, Title = "Water the plants", Description = "Indoor and outdoor plants" },
            new Todo { Id = 15, Title = "Learn a new recipe", Description = "Try making homemade pizza" },
            new Todo { Id = 16, Title = "Visit the dentist", Description = "Schedule a check-up" },
            new Todo { Id = 17, Title = "Write thank-you notes", Description = "For birthday gifts" },
            new Todo { Id = 18, Title = "Buy a gift", Description = "Friend's wedding present" },
            new Todo { Id = 19, Title = "Review monthly budget", Description = "Track expenses" },
            new Todo { Id = 20, Title = "Practice guitar", Description = "Learn a new chord progression" }
        };
    
        [HttpGet]
        public ActionResult<IEnumerable<Todo>> Get()
        {
            return Ok(_todos);
        }

        [HttpGet("{id}")]
        public ActionResult<Todo> Get(int id)
        {
            return _todos.Any(t => t.Id == id) ? Ok(_todos.First(t => t.Id == id)) 
                : NotFound();
        }

        [HttpPost]
        public IActionResult Post(Todo todo)
        {
            todo.Id = _todos.Count + 1;
            _todos.Add(todo);
            return Created("api/v1/todos/" + todo.Id, todo);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, Todo todo)
        {
            if(id != todo.Id)
                return BadRequest();
            if (!_todos.Any(t => t.Id == id))
                return NotFound();

            var existingTodo = _todos.Find(t => t.Id == id);
            existingTodo.Description = todo.Description;
            existingTodo.Title = todo.Title;

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!_todos.Any(t => t.Id == id))
                return NotFound();
            _todos.Remove(_todos.Find(t => t.Id == id));

            return NoContent();
        }
    }
}
