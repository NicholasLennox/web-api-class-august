﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using WebApiClass2023.Data;
using WebApiClass2023.Data.Exceptions;
using WebApiClass2023.Data.Models;
using WebApiClass2023.Services.Professors;

namespace WebApiClass2023.Controllers
{
    // Configuring base route, [controller] uses tokenization to replace with the class name
    [Route("api/v1/[controller]")]
    [ApiController] // Enables all the JSON and status code erorr handling
    public class ProfessorsController : ControllerBase
    {
        // Want to separate out our converns
        // This lets us keep data access, business logic, and user interaction separate
        private readonly IProfessorService _profService;

        public ProfessorsController(IProfessorService profService)
        {
            _profService = profService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Professor>>> GetProfessors()
        {
            return Ok(await _profService.GetAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Professor>> GetProfessor(int id)
        {
            try
            {
                return await _profService.GetByIdAsync(id);
            } catch(ProfessorNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutProfessor(int id, Professor professor)
        {
            if (id != professor.Id)
            {
                return BadRequest();
            }

            try
            {
                await _profService.UpdateAsync(professor);
            } catch (ProfessorNotFoundException ex)
            {
                return NotFound(ex.Message);
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Professor>> PostProfessor(Professor professor)
        {
           await _profService.AddAsync(professor);

           return CreatedAtAction("GetProfessor", new { id = professor.Id }, professor);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProfessor(int id)
        {
            try
            {
                await _profService.DeleteAsync(id);
                return NoContent();
            } catch (ProfessorNotFoundException ex)
            {
                return NotFound(ex.Message);
            } 
        }
    }
}
