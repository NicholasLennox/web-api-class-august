using Microsoft.EntityFrameworkCore;
using WebApiClass2023.Data;
using WebApiClass2023.Services.Professors;

namespace WebApiClass2023
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Special extension method to register DbContext in services
            builder.Services.AddDbContext<PostgradDbContext>(options 
                => options.UseSqlServer(builder.Configuration.GetConnectionString("Postgrad"))); // Get connection string from appsettings.json
            // We register our ProfessorService to DI,
            // this lets us ask for it in our classes and ASP.NET creates the instances for us
            builder.Services.AddScoped<IProfessorService, ProfessorService>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}