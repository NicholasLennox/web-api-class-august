﻿using WebApiClass2023.Services.Professors;
namespace WebApiClass2023.Services
{
    // The inheritdoc is what I have to do to get the professor specific comments in the interface.
    // Im sure there is a better way to do this, but for now, this is the solution.
    // It maybe makes sense for the IProfessorService, but I really dont like it here - as IStudentService will also have these docs.
    // Maybe there is a way to have a seprate documentation configuration that I can override in the IProfessorService
    public interface ICrudService <T, ID>
    {
        /// <inheritdoc cref="ProfessorService.GetAllAsync"/>
        Task<IEnumerable<T>> GetAllAsync();

        /// <inheritdoc cref="ProfessorService.GetByIdAsync(int)"/>
        Task<T> GetByIdAsync(ID id);

        /// <inheritdoc cref="ProfessorService.AddAsync(Data.Models.Professor)"/>
        Task<T> AddAsync(T obj);

        /// <inheritdoc cref="ProfessorService.UpdateAsync(Data.Models.Professor)"/>
        Task<T> UpdateAsync(T obj);

        /// <inheritdoc cref="ProfessorService.DeleteAsync(int)"/>
        Task DeleteAsync(ID id);
    }
}
