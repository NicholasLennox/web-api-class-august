﻿using WebApiClass2023.Data.Models;

namespace WebApiClass2023.Services.Professors
{
    public interface IProfessorService : ICrudService<Professor, int>
    {
        /// <inheritdoc cref="ProfessorService.AddStudentAsync(int, int)"/>
        Task AddStudentAsync(int studentId, int professorId);

        /// <inheritdoc cref="ProfessorService.GetSubjectsAsync(int)"/>
        Task<ICollection<Subject>> GetSubjectsAsync(int id);

        /// <inheritdoc cref="ProfessorService.AddSubjectAsync(int, int)"/>
        Task AddSubjectAsync(int subjectId, int professorId);
    }
}
