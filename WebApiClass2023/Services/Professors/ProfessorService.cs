﻿using Microsoft.EntityFrameworkCore;
using WebApiClass2023.Data;
using WebApiClass2023.Data.Exceptions;
using WebApiClass2023.Data.Models;

namespace WebApiClass2023.Services.Professors
{
    // We have to manually add async to our methods here, we cant specify it in an interface
    public class ProfessorService : IProfessorService
    {
        private readonly PostgradDbContext _context;

        public ProfessorService(PostgradDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Saves a new professor to the database.
        /// If you add related entities (students, subjects) the business logic rules wont apply.
        /// This is something you can change by forcing .Clear() on the professor.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<Professor> AddAsync(Professor obj)
        {
            // We dont want the ability to add related data in a single post
            // This clearing problem goes away once we have DTOs
            obj.Subjects.Clear();
            obj.Students.Clear();
            await _context.Professors.AddAsync(obj);
            await _context.SaveChangesAsync();
            return obj;
        }

        /// <summary>
        /// Registers a new student for a professor to supervise.
        /// Professors can supervise a maximum of 5 students at a time.
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="professorId"></param>
        /// <returns></returns>
        /// <exception cref="ProfessorNotFoundException"></exception>
        /// <exception cref="StudentNotFoundException"></exception>
        /// <exception cref="ProfessorException"></exception>
        public async Task AddStudentAsync(int studentId, int professorId)
        {
            // Early exists/gaurds
            if (!await ProfessorExistsAsync(professorId))
                FailWithProfessorNotFound(professorId);
            if (!await StudentExistsAsync(studentId))
                FailWithStudentNotFound(studentId);

            // Proceed with "good path"
            var professor = await _context.Professors
                .Where(p => p.Id == professorId)
                .Include(p => p.Students)
                .FirstAsync();

            // Business logic gaurd
            if (professor.Students is null)
                professor.Students = new List<Student>();

            if (professor.Students.Count >= 5)
                FailWithTooManyStudents();

            // All good, we can update
            var student = await _context.Students.FirstAsync(s => s.Id == studentId);
            professor.Students.Add(student);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets the subjects for a professor. 
        /// Throws an exception if the professor does not exist.
        /// If there are no subjects, no exception is thrown, an empty set is returned.
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="professorId"></param>
        /// <returns></returns>
        /// <exception cref="ProfessorNotFoundException"></exception>
        /// <exception cref="SubjectNotFoundException"></exception>
        /// <exception cref="ProfessorException"></exception>
        public async Task AddSubjectAsync(int subjectId, int professorId)
        {
            // Early exists/gaurds
            if (!await ProfessorExistsAsync(professorId))
                FailWithProfessorNotFound(professorId);
            if (!await SubjectExistsAsync(subjectId))
                FailWithSubjectNotFound(subjectId);

            // Proceed with "good path"
            var professor = await _context.Professors
                .Where(p => p.Id == professorId)
                .Include(p => p.Subjects)
                .FirstAsync();

            // Business logic gaurd
            if (professor.Subjects is null)
                professor.Subjects = new List<Subject>();

            if (professor.Subjects.Count >= 3)
                FailWithTooManySubjects();

            // All good, we can update
            var subject = await _context.Subjects.FirstAsync(s => s.Id == subjectId);
            professor.Subjects.Add(subject);
            await _context.SaveChangesAsync();
        }

        /// <inheritdoc/>
        /// <summary>
        /// Deletes a Professor. The relationships with subjects and students are severed (set null).
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="ProfessorNotFoundException"></exception>
        public async Task DeleteAsync(int id)
        {
            // Early exit if prof doesnt exist
            if (!await ProfessorExistsAsync(id))
                FailWithProfessorNotFound(id);

            // We do the null check in the controller
            var prof = await _context.Professors
                .Where(p => p.Id == id)
                .FirstAsync();

            // Want to remove related entities to not cause referential problems
            // https://learn.microsoft.com/en-us/ef/core/saving/cascade-delete#severing-a-relationship
            // Only works if relationship is nullable
            prof.Students.Clear();
            prof.Subjects.Clear();
            // Can safely remove professor
            _context.Professors.Remove(prof); // Doesnt seen to be a RemoveAsync method
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Get all professors from the database.
        /// Does not include related entities.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Professor>> GetAllAsync()
        {
            return await _context.Professors.ToListAsync();
        }

        /// <summary>
        /// Gets a professor by their ID.
        /// Throws an exception if the professor does not exist in the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="ProfessorNotFoundException"></exception>
        public async Task<Professor> GetByIdAsync(int id)
        {
            var prof = await _context.Professors.Where(p => p.Id == id).FirstAsync();

            if (prof is null)
                FailWithProfessorNotFound(id);

            return prof;
        }

        /// <summary>
        /// Gets the subjects for a professor. 
        /// Throws an exception if the professor does not exist.
        /// If there are no subjects, no exception is thrown, an empty set is returned.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ICollection<Subject>> GetSubjectsAsync(int id)
        {
            // Its a smaller SQL statement if we go from subjects (no joining)
            return await _context.Subjects
                .Where(s => s.ProfessorId == id)
                .ToListAsync();
        }

        /// <summary>
        /// If you add related entities (students, subjects) the business logic rules wont apply.
        /// This is something you can change by forcing .Clear() on the professor.
        /// You can create separate methods to update the related entities.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// <exception cref="ProfessorNotFoundException"></exception>
        public async Task<Professor> UpdateAsync(Professor obj)
        {
            if (!await ProfessorExistsAsync(obj.Id))
                FailWithProfessorNotFound(obj.Id);

            // We want to force updating related entities in another endpoint
            obj.Students.Clear();
            obj.Students.Clear();
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();

            return obj;
        }

        // "Hjelper Meg" methods
        private async Task<bool> ProfessorExistsAsync(int id)
        {
            return await _context.Professors.AnyAsync(p => p.Id == id);
        }

        private Task<bool> StudentExistsAsync(int studentId)
        {
            return _context.Students.AnyAsync(s => s.Id == studentId);
        }

        private Task<bool> SubjectExistsAsync(int subjectId)
        {
            return _context.Subjects.AnyAsync(s => s.Id == subjectId);
        }

        // Trying a new way of giving expressions names to help readability
        // Not sure how I feel about it, give me your opinions
        private static void FailWithProfessorNotFound(int id)
        {
            throw new ProfessorNotFoundException(id);
        }

        private static void FailWithSubjectNotFound(int subjectId)
        {
            throw new SubjectNotFoundException(subjectId);
        }

        private static void FailWithTooManySubjects()
        {
            throw new ProfessorException("Professor has too many subjects");
        }

        private static void FailWithTooManyStudents()
        {
            throw new ProfessorException("Professor has too many students");
        }

        private static void FailWithStudentNotFound(int studentId)
        {
            throw new StudentNotFoundException(studentId);
        }
    }
}
