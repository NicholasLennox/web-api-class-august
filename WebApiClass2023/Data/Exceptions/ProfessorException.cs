﻿namespace WebApiClass2023.Data.Exceptions
{
    public class ProfessorException : Exception
    {
        public ProfessorException(string? message) : base(message)
        {
        }
    }
}
