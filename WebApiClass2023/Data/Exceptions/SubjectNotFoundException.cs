﻿namespace WebApiClass2023.Data.Exceptions
{
    public class SubjectNotFoundException : Exception
    {
        public SubjectNotFoundException(int id)
            : base($"Subject does not with with ID: {id}")
        {
        }
    }
}
