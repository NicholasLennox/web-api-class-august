﻿namespace WebApiClass2023.Data.Exceptions
{
    public class StudentNotFoundException : Exception
    {
        public StudentNotFoundException(int id)
            : base($"Student does not with with ID: {id}")
        {
        }
    }
}
