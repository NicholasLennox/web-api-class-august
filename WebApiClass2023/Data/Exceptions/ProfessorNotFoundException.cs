﻿namespace WebApiClass2023.Data.Exceptions
{
    public class ProfessorNotFoundException : Exception
    {
        public ProfessorNotFoundException(int id) 
            : base($"Professor does not with with ID: {id}")
        {
        }
    }
}
